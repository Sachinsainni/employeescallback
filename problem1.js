let fs = require('fs');
const { resourceUsage } = require('process');

function readData() {
    let readPromise = new Promise((resolve, reject) => {
        fs.readFile('data.json', 'utf-8', (err, data) => {
            if (err) {
                reject(err);
            } else {
                console.log('read file successfully');
                resolve(data);
            }
        })
    })
    return readPromise;
}



// 1. Retrieve data for ids : [2, 13, 23].

function retrieveData(data) {


    return new Promise((resolve, reject) => {
        let dataTojson = JSON.parse(data)
        let array = dataTojson["employees"]
        let result = array.filter(obj => {
            return obj.id == 2 || obj.id == 13 || obj.id == 23;

        })
        console.log(result);
        fs.writeFile('data/retrieveIDs.json', JSON.stringify(result), (err, data) => {
            if (err) {
                reject(err);
            } else {
                resolve('write data successfully')
            }

        })
    })
}






readData().then((data) => {
    // console.log(data);
    return retrieveData(data);

}).then((data)=>{
    console.log(data);
})
.catch((err) => {
    console.error(err);
})